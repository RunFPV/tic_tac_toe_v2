﻿using System;

namespace TicTacToe.Models.Services.Authentication
{
    public class AuthenticationRequest
    {
        public string UserName { get; set; }

        public string Password { get; set; }

        public void Validate()
        {
            if (string.IsNullOrWhiteSpace(UserName))
                throw new ArgumentNullException(nameof(UserName));

            if (string.IsNullOrWhiteSpace(Password))
                throw new ArgumentNullException(nameof(Password));
        }
    }
}
