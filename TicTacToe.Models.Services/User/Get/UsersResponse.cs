﻿using System.Collections.Generic;

namespace TicTacToe.Models.Services.User.Get
{
    public class UsersResponse
    {
        public IEnumerable<UserResponse> Users { get; set; }
    }
}
