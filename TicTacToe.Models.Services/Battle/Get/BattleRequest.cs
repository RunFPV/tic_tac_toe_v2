﻿using System;
using TicTacToe.Common;

namespace TicTacToe.Models.Services.Battle.Get
{
    public class BattleRequest
    {
        public string UserName { get; set; }

        public DateTime? BeginDate { get; set; }

        public DateTime? EndDate { get; set; }

        public ResultGame? Result { get; set; }

        public void Validate()
        {
            if (
                Result.HasValue &&
                (Result.Value == ResultGame.Player1Win || Result.Value == ResultGame.Player1Lose) &&
                string.IsNullOrWhiteSpace(UserName))
                throw new Exception($"{nameof(UserName)} is mandatory if {nameof(Result)} is present");
        }
    }
}
