﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TicTacToe.Common
{
    public class CommonTools
    {
        public static string GetSHA512Hash(string input)
        {
            var sha512 = System.Security.Cryptography.SHA512.Create();

            // Convert the input string to a byte array and compute the hash.
            byte[] data = sha512.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }
    }
}
