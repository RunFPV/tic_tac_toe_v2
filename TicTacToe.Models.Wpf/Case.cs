﻿using System;

namespace TicTacToe.Models.Wpf
{
    /// <summary>
    /// Objet case
    /// </summary>
    public class Case : BaseModel
    {
        int _id;
        bool _value;
        Uri _path;

        /// <summary>
        /// Id de la case
        /// </summary>
        public int Id
        {
            get => _id;
            set
            {
                _id = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Valeur de la case
        /// </summary>
        public bool Value
        {
            get => _value;
            set
            {
                _value = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Path de l'image
        /// </summary>
        public Uri Path
        {
            get => _path;
            set
            {
                _path = value;
                OnPropertyChanged();
            }
        }
    }
}
