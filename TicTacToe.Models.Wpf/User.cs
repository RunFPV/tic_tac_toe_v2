﻿using System.Collections.Generic;

namespace TicTacToe.Models.Wpf
{
    /// <summary>
    /// Objet utilisateur
    /// </summary>
    public class User : BaseModel
    {
        string _nom;
        int _score;
        List<int> _coup;

        /// <summary>
        /// Nom de l'utilisateur
        /// </summary>
        public string Nom
        {
            get => _nom;
            set
            {
                _nom = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Score de l'utilisateur
        /// </summary>
        public int Score
        {
            get => _score;
            set
            {
                _score = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Coups de l'utilisateur
        /// </summary>
        public List<int> Coup
        {
            get => _coup;
            set
            {
                _coup = value;
                OnPropertyChanged();
            }
        }
    }
}
