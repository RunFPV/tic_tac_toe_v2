﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;
using Get = TicTacToe.Models.Services.User.Get;
using Post = TicTacToe.Models.Services.User.Post;

namespace WebApp_TicTacToe.Controllers
{
    public class HomeController : Controller
    {
        readonly string Baseurl = "https://localhost:44383/";

        public async Task<ActionResult> Index()
        {
            var users = new Get.UsersResponse();

            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync("api/user");

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    //Storing the response details recieved from web api   
                    var response = Res.Content.ReadAsStringAsync().Result;

                    //Deserializing the response recieved from web api and storing into the Employee list  
                    users = JsonConvert.DeserializeObject<Get.UsersResponse>(response);

                }
                //returning the employee list to view  
                return View(users);
            }
        }


        //public ActionResult Index(UserModel userModel)
        //{
        //    return View(userModel);
        //}

        [HttpGet]
        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> RegisterAsync(Post.UserRequest userModel)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.PostAsJsonAsync("api/user", userModel);

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }

                ViewData["Error"] = "Une erreur est survenue";
                return View("Register");
            }
        }
    }
}