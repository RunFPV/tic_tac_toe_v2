﻿using System;
using TicTacToe.Common;

namespace TicTacToe.Models
{
    public class BattleModel : BaseModel
    {
        //public int Player1Id { get; set; }
        public UserModel Player1 { get; set; }

        //public int Player2Id { get; set; }
        public UserModel Player2 { get; set; }

        public DateTime FightDate { get; set; }
        public ResultGame Result { get; set; }
    }
}
