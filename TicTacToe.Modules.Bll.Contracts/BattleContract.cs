﻿using System;
using System.Composition;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Entity;
using TicTacToe.Interfaces.Bll.Contracts;
using Get = TicTacToe.Models.Services.Battle.Get;
using Post = TicTacToe.Models.Services.Battle.Post;

namespace TicTacToe.Modules.Bll.Contracts
{
    [Export(typeof(IBattleContract))]
    public class BattleContract : IBattleContract
    {
        #region Private Property
        private readonly DataContext context;
        #endregion

        #region Constructor
        /// <summary>
        /// Controller Contract
        /// </summary>
        [ImportingConstructor]
        public BattleContract(DataContext context)
        {
            this.context = context;
        }
        #endregion


        #region Public Method
        public Task<Get.BattlesResponse> GetAsync(Get.BattleRequest request)
        {
            return Task.Factory.StartNew(() =>
            {
                return Get(request);
            });
        }

        public Task PostAsync(Post.BattleRequest request)
        {
            return Task.Factory.StartNew(() =>
            {
                Post(request);
            });
        }
        #endregion


        #region Private Method
        private Get.BattlesResponse Get(Get.BattleRequest request)
        {
            request.Validate();

            var battles = context.Battle.Select(e => e);

            if (!string.IsNullOrWhiteSpace(request.UserName))
                battles = battles.Where(u => u.Player1.UserName == request.UserName || u.Player1.UserName == request.UserName);

            if (request.BeginDate.HasValue)
                battles = battles.Where(u => DateTime.Compare(request.BeginDate.Value, u.FightDate) <= 0);

            if (request.EndDate.HasValue)
                battles = battles.Where(u => DateTime.Compare(request.EndDate.Value, u.FightDate) >= 0);

            if (request.Result.HasValue)
                battles = battles.Where(u => u.Result == request.Result.Value);

            return new Get.BattlesResponse
            {
                Battles = battles.Select(e => new Get.BattleResponse
                {
                    Player1 = e.Player1.UserName,
                    Player2 = e.Player2.UserName,
                    Date = e.FightDate,
                    Result = e.Result
                })
            };
        }

        private void Post(Post.BattleRequest request)
        {
            var user1 = context.User.SingleOrDefault(e => e.UserName == request.User1);
            var user2 = context.User.SingleOrDefault(e => e.UserName == request.User2);

            if (user1 == null)
                throw new Exception($"The User {request.User1} doesn't exist.");
            if (user2 == null)
                throw new Exception($"The User {request.User2} doesn't exist.");

            context.Battle.Add(new Models.BattleModel
            {
                Player1 = user1,
                Player2 = user2,
                FightDate = request.Date ?? DateTime.UtcNow,
                Result = request.Result,
            });

            context.SaveChanges();
        }
        #endregion
    }
}
