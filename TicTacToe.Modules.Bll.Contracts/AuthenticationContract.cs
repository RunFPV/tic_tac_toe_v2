﻿using System;
using System.Composition;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Common;
using TicTacToe.Entity;
using TicTacToe.Interfaces.Bll.Contracts;
using TicTacToe.Models.Services.Authentication;

namespace TicTacToe.Modules.Bll.Contracts
{
    public class AuthenticationContract : IAuthenticationContract
    {
        #region Private Property
        private readonly DataContext context;
        #endregion

        #region Constructor
        /// <summary>
        /// Controller Contract
        /// </summary>
        [ImportingConstructor]
        public AuthenticationContract(DataContext context)
        {
            this.context = context;
        }
        #endregion

        #region Public Method
        public Task<AuthenticationResponse> AuthenticationAsync(AuthenticationRequest request)
        {
            return Task.Factory.StartNew(() =>
            {
                return Authentication(request);
            });
        }
        #endregion

        #region Private Method
        private AuthenticationResponse Authentication(AuthenticationRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            var user = context.User.SingleOrDefault(u => u.UserName == request.UserName);

            if (user == null)
                throw new Exception($"The User {request.UserName} doesn't exist.");

            if (!user.Password.Equals(CommonTools.GetSHA512Hash(request.Password)))
                throw new Exception($"The Password of the user {request.UserName} is invalid.");

            return new AuthenticationResponse
            {
                UserName = user.UserName,
                Authenticated = true
            };
        }
        #endregion
    }
}
