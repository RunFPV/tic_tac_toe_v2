﻿using Microsoft.EntityFrameworkCore;
using TicTacToe.Models;

namespace TicTacToe.Entity
{
    public class DataContext : DbContext
    {
        public DbSet<UserModel> User { get; set; }
        public DbSet<BattleModel> Battle { get; set; }

        #region Constructor
        //public DataContext() : base()
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }
        #endregion

        #region Override Method
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserModel>(entity =>
            {
                entity.HasIndex(p => p.UserName).IsUnique();
            });
        }
        #endregion

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;
        //       Database=Morpion;Trusted_Connection=True;ConnectRetryCount=0");
        //}
    }
}
