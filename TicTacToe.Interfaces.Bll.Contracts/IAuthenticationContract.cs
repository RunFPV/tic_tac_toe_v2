﻿using System.Threading.Tasks;
using TicTacToe.Models.Services.Authentication;

namespace TicTacToe.Interfaces.Bll.Contracts
{
    public interface IAuthenticationContract
    {
        Task<AuthenticationResponse> AuthenticationAsync(AuthenticationRequest request);
    }
}
