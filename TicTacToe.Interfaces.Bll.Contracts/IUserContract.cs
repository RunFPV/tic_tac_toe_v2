﻿using System.Threading.Tasks;
using Delete = TicTacToe.Models.Services.User.Delete;
using Get = TicTacToe.Models.Services.User.Get;
using Post = TicTacToe.Models.Services.User.Post;
using Put = TicTacToe.Models.Services.User.Put;

namespace TicTacToe.Interfaces.Bll.Contracts
{
    public interface IUserContract
    {
        Task<Get.UsersResponse> GetAsync(Get.UserRequest request);

        Task PostAsync(Post.UserRequest request);

        Task PutAsync(Put.UserRequest request);

        Task DeleteAsync(Delete.UserRequest request);
    }
}
