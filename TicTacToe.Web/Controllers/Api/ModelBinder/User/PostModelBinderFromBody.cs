﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicTacToe.Models.Services.User.Post;

namespace TicTacToe.Web.Controllers.Api.ModelBinder.User
{
    public class PostModelBinderFromBody : IModelBinder
    {
        Task IModelBinder.BindModelAsync(ModelBindingContext bindingContext)
        {
            if (bindingContext == null)
                throw new ArgumentNullException(nameof(bindingContext));

            var request = new UserRequest();

            try
            {
                var content = bindingContext.ActionContext.HttpContext.Request.Body;
                //var content = bindingContext.Request.Content;
                string json = content.ToString();
                //dynamic tr = JsonConvert.DeserializeObject(json);

                //if (tr != null)
                //{
                //    if (tr.UserName is Newtonsoft.Json.Linq.JValue)
                //        request.UserName = tr.UserName;

                //    if (tr.Password is Newtonsoft.Json.Linq.JValue)
                //        request.Password = tr.Password;

                //    if (tr.FirstName is Newtonsoft.Json.Linq.JValue)
                //        request.FirstName = tr.FirstName;

                //    if (tr.LastName is Newtonsoft.Json.Linq.JValue)
                //        request.LastName = tr.LastName;

                //    if (tr.Email is Newtonsoft.Json.Linq.JValue)
                //        request.Email = tr.Email;
                //}
            }
            catch (JsonReaderException e)
            {
                throw new Exception(e.Message, e);
            }

            request.Validate();

            bindingContext.Result = ModelBindingResult.Success(request);

            return Task.CompletedTask;
        }
    }
}