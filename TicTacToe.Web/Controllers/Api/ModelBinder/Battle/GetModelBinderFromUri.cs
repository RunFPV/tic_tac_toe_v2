﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Threading.Tasks;
using TicTacToe.Common;
using TicTacToe.Models.Services.Battle.Get;

namespace TicTacToe.Web.Controllers.Api.ModelBinder.Battle
{
    public class GetModelBinderFromUri : IModelBinder
    {
        Task IModelBinder.BindModelAsync(ModelBindingContext bindingContext)
        {
            return Task.Factory.StartNew(() =>
                {
                    if (bindingContext == null)
                        throw new ArgumentNullException(nameof(bindingContext));

                    var request = new BattleRequest();

                    var userName = bindingContext.ValueProvider.GetValue(nameof(BattleRequest.UserName));
                    request.UserName = userName.Values;

                    var beginDateJson = bindingContext.ValueProvider.GetValue(nameof(BattleRequest.BeginDate)).Values;
                    if (!string.IsNullOrWhiteSpace(beginDateJson))
                    {
                        DateTime beginDate;
                        DateTime.TryParse(beginDateJson, out beginDate);
                        request.BeginDate = beginDate;
                    }

                    var endDateJson = bindingContext.ValueProvider.GetValue(nameof(BattleRequest.EndDate)).Values;
                    if (!string.IsNullOrWhiteSpace(endDateJson))
                    {
                        DateTime endDate;
                        DateTime.TryParse(endDateJson, out endDate);
                        request.EndDate = endDate;
                    }

                    var resultJson = bindingContext.ValueProvider.GetValue(nameof(BattleRequest.Result)).Values;
                    if (!string.IsNullOrWhiteSpace(resultJson))
                    {
                        ResultGame result;
                        Enum.TryParse(resultJson, out result);
                        request.Result = result;
                    }

                    //bindingContext.Model = request;
                    bindingContext.Result = ModelBindingResult.Success(request);

                    return Task.CompletedTask;
                });
        }
    }
}
