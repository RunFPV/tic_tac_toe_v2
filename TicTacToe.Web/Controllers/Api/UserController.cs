﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Composition;
using System.Threading.Tasks;
using TicTacToe.Interfaces.Bll.Contracts;
using TicTacToe.Web.Controllers.Api.ModelBinder.User;
using Delete = TicTacToe.Models.Services.User.Delete;
using Get = TicTacToe.Models.Services.User.Get;
using Post = TicTacToe.Models.Services.User.Post;
using Put = TicTacToe.Models.Services.User.Put;

namespace TicTacToe.Web.Controllers.Api
{
    [Route("Api/[Controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        #region Private Property
        private readonly IUserContract contract;
        #endregion

        #region Contructor
        [ImportingConstructor]
        public UserController(IUserContract contract)
        {
            if (contract == null)
                throw new ArgumentNullException(nameof(contract));

            this.contract = contract;
        }

        #endregion

        #region Public Method
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Get.UsersResponse> Get([ModelBinder(typeof(GetModelBinderFromUri))]Get.UserRequest request)
        {
            return await contract.GetAsync(request);
        }

        /// <summary>
        /// Post
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task Post(Post.UserRequest request)
        {
            await contract.PostAsync(request);
        }

        /// <summary>
        /// Put
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task Put(Put.UserRequest request)
        {
            await contract.PutAsync(request);
        }

        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task Delete([ModelBinder(typeof(DeleteModelBinderFromUri))]Delete.UserRequest request)
        {
            await contract.DeleteAsync(request);
        }
        #endregion
    }
}
