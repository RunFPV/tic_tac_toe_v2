﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Composition;
using System.Threading.Tasks;
using TicTacToe.Interfaces.Bll.Contracts;
using TicTacToe.Web.Controllers.Api.ModelBinder.Battle;
using Get = TicTacToe.Models.Services.Battle.Get;
using Post = TicTacToe.Models.Services.Battle.Post;
namespace TicTacToe.Web.Controllers.Api
{
    [Route("Api/[Controller]")]
    [ApiController]
    public class BattleController : ControllerBase
    {
        #region Private Property
        private readonly IBattleContract contract;
        #endregion

        #region Contructor
        [ImportingConstructor]
        public BattleController(IBattleContract contract)
        {
            if (contract == null)
                throw new ArgumentNullException(nameof(contract));

            this.contract = contract;
        }
        #endregion

        #region Public Method
        /// <summary>
        /// Get
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Get.BattlesResponse> Get([ModelBinder(typeof(GetModelBinderFromUri))]Get.BattleRequest request)
        {
            return await contract.GetAsync(request);
        }

        /// <summary>
        /// Post
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task Post(Post.BattleRequest request)
        {
            await contract.PostAsync(request);
        }
        #endregion
    }
}
