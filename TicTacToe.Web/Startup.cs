﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TicTacToe.Entity;
using TicTacToe.Interfaces.Bll.Contracts;
using TicTacToe.Modules.Bll.Contracts;
using TicTacToe.Modules.SignalRGame.Hubs;

namespace TicTacToe.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<IUserContract, UserContract>();
            services.AddScoped<IBattleContract, BattleContract>();
            services.AddScoped<IAuthenticationContract, AuthenticationContract>();

            string connection = @"Server=(localdb)\mssqllocaldb;Database=Morpion;Trusted_Connection=True;ConnectRetryCount=0";
            services.AddDbContext<DataContext>(options => options.UseSqlServer(connection));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddSignalR();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSignalR(routes =>
            {
                routes.MapHub<GameHub>("/gameHub");
            });
            //app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
