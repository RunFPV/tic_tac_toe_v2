﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wpf_TicTacToe.ViewModels;

namespace Wpf_TicTacToe
{
    /// <summary>
    /// Logique d'interaction pour Menu.xaml
    /// </summary>
    public partial class Menu : Page
    {

        public static string user1;
        public static string user2;
        MenuViewModel _menuViewModel = new MenuViewModel();
        public Menu()
        {
            InitializeComponent();
            DataContext = _menuViewModel;
        }

        private void ButtonPlay_Click(object sender, RoutedEventArgs e)
        {
            if (!(txtUser1.Text == "" || txtUser2.Text == "") && txtUser1.Text != txtUser2.Text) {
                user1 = txtUser1.Text;
                user2 = txtUser2.Text;
                NavigationService.Navigate(new Uri(@"Views\Game.xaml", UriKind.Relative));
            } else {
                MessageBox.Show("Les pseudos ne sont pas correctement saisis ou sont égaux");
            }
        }

        private void ButtonStats_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri(@"Views\Stats.xaml", UriKind.Relative));
        }

        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri(@"Views\Home.xaml", UriKind.Relative));

        }
    }
}
