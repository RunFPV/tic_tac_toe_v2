﻿using System;
using System.Diagnostics;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Wpf_TicTacToe.Hub;
using Wpf_TicTacToe.ViewModels;

namespace Wpf_TicTacToe.Views
{
    /// <summary>
    /// Logique d'interaction pour Home.xaml
    /// </summary>
    public partial class Home : Page
    {
        HomeViewModel _homeViewModel = new HomeViewModel();
        public ClientHub connection;

        public Home()
        {
            new ClientHub();
            InitializeComponent();
            DataContext = _homeViewModel;
        }

        private async void ButtonOnline_Click(object sender, RoutedEventArgs e)
        {
            if ((!string.IsNullOrEmpty(txtPseudo.Text) || !string.IsNullOrEmpty(txtPassword.Text)) && await Authentication(txtPseudo.Text, txtPassword.Text))
            {
                NavigationService.Navigate(new Uri(@"Views\Lobby.xaml", UriKind.Relative));

                ClientHub.StartHubAsync(txtPseudo.Text);
                ClientHub.ConnectAsync();
            }
        }

        private void ButtonLocal_Click(object sender, RoutedEventArgs e)
        {
            NavigationService.Navigate(new Uri(@"Views\Menu.xaml", UriKind.Relative));
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }


        private async Task<bool> Authentication(string login, string password)
        {
            using (var client = new HttpClient())
            {
                string Baseurl = "https://localhost:44383/";

                //Passing service base url  
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                //Define request data format  
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //Sending request to find web api REST service resource GetAllEmployees using HttpClient  
                HttpResponseMessage Res = await client.GetAsync($"api/authentication?UserName={login}&Password={password}");

                //Checking the response is successful or not which is sent using HttpClient  
                if (Res.IsSuccessStatusCode)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
