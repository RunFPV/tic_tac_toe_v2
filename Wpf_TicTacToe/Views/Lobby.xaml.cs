﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TicTacToe.Models.Services.Hubs;
using Wpf_TicTacToe.Hub;
using Wpf_TicTacToe.ViewModels;

namespace Wpf_TicTacToe.Views
{
    /// <summary>
    /// Logique d'interaction pour Lobby.xaml
    /// </summary>
    public partial class Lobby : Page
    {
        LobbyViewModel _LobbyViewModel = new LobbyViewModel();

        public Lobby()
        {
            InitializeComponent();
            DataContext = _LobbyViewModel;
            InitializeHubMethod();
        }

        #region Map XAML Reference
        private void ButtonBack_Click(object sender, RoutedEventArgs e)
        {
            ClientHub.DisconnectAsync();
            NavigationService.Navigate(new Uri(@"Views\Home.xaml", UriKind.Relative));
        }

        private void ButtonRefresh_Click(object sender, RoutedEventArgs e)
        {
            ClientHub.RefleshListAsync();
        }

        private void ListViewItem_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var item = sender as ListViewItem;
            if (item != null && item.IsSelected)
            {
                MessageBox.Show($"Wait response of {item.Content.ToString()}", $"AskFight {ClientHub.UserName}");
                ClientHub.FightRequestAsync(item.Content.ToString());
            }
        } 
        #endregion

        private void InitializeHubMethod()
        {
            ClientHub.Connection.On<IEnumerable<string>>("PlayerList", (players) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    _LobbyViewModel.ListPlayer = players.Where(e=>e != ClientHub.UserName).ToList();
                });
            });

            ClientHub.Connection.On<Battle>("AcceptBattle", (battle) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    var choice = MessageBox.Show($"Do you accept to fight {battle._NamePlayerAsk} ?", $"FightRequest {ClientHub.UserName}", MessageBoxButton.YesNo);
                    battle._Accepted = choice.Equals(MessageBoxResult.Yes);
                    ClientHub.FightResponseAsync(battle);
                });
            });

            ClientHub.Connection.On<string>("Reject", (userName) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    MessageBox.Show($"{userName} refused your fight request.", $"FightRefused {ClientHub.UserName}");
                    NavigationService.Navigate(new Uri(@"Views\Lobby.xaml", UriKind.Relative));
                });
            });

            ClientHub.Connection.On<Battle>("BeginFight", (battle) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    MessageBox.Show($"BeginFight between {battle._NamePlayerAsk} and {battle._NamePlayerCall}.", $"BeginFight {ClientHub.UserName}");
                    NavigationService.Navigate(new Uri(@"Views\Game.xaml", UriKind.Relative));
                });
            });

            ClientHub.Connection.On<Battle>("ChangePlayerTurn", (battle) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    MessageBox.Show($"It's turn of {battle._ActivePlayer}.", $"ChangePlayerTurn {ClientHub.UserName}");
                });
            });
        }
    }
}
