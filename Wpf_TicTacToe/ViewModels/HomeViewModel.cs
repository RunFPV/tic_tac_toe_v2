﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Wpf_TicTacToe.Models;

namespace Wpf_TicTacToe.ViewModels
{
    public class HomeViewModel : BaseModel
    {
        //Variables publiques
        public Uri UriBackground
        {
            get => new Uri("Resources/Pictures/MenuBackground.png", UriKind.Relative);
        }
    }
}
