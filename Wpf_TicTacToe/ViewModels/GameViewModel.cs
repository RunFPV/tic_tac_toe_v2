﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using Wpf_TicTacToe.Models;

namespace Wpf_TicTacToe.ViewModels
{
    public class GameViewModel : BaseModel
    {
        //Variables privées
        User _user1 = new User();
        User _user2 = new User();
        bool _isJoueur1;
        StatsViewModel _statsViewModel = new StatsViewModel();
        List<Case> _listeCase = new List<Case>();

        //Variables publiques

        public Uri UriBackground
        {
            get => new Uri("Resources/Pictures/gameBackground.png", UriKind.Relative);
        }
        public User User1
        {
            get => _user1;
            set
            {
                _user1 = value;
                OnPropertyChanged();
            }
        }
        public User User2
        {
            get => _user2;
            set
            {
                _user2 = value;
                OnPropertyChanged();
            }
        }
        public bool IsJoueur1
        {
            get => _isJoueur1;
            set
            {
                _isJoueur1 = value;
                OnPropertyChanged();
            }
        }
        public List<Case> ListeCase
        {
            get => _listeCase;
            set
            {
                _listeCase = value;
                OnPropertyChanged();
            }
        }

        /// <summary>
        /// Constructeur
        /// </summary>
        public GameViewModel()
        {
            InitGame();
        }

        /// <summary>
        /// Initialise le début de partie (RAZ)
        /// </summary>
        public void InitGame()
        {
            //Initialisation user1
            User1 = new User()
            {
                Nom = Menu.user1,
                Score = 0,
                Coup = new List<int>()
            };

            //Initialisation user1
            User2 = new User()
            {
                Nom = Menu.user2,
                Score = 0,
                Coup = new List<int>()
            };

            //Initialisation Cases
            ListeCase = Enumerable.Range(1, 9).Select((i) =>
            {
                return new Case()
                {
                    Id = i,
                    Value = false,
                    Path = new Uri("Resources/Pictures/blank.png", UriKind.Relative)
                };
            }).ToList();


            //ListeCase = new List<Case>();
            //for (int i = 1; i < 10; i++)
            //{
            //    ListeCase.Add(new Case()
            //    {
            //        Id = i,
            //        Value = false,
            //        Path = new Uri("Resources/Pictures/background.png", UriKind.Relative)
            //    });
            //}

            //Initialisation 1er joueur
            IsJoueur1 = GetFirstUser();
        }

        /// <summary>
        /// Appelée lorsqu'un utilisateur joue
        /// </summary>
        /// <param name="sender"></param>
        public void CaseClick(Button btn)
        {
            int btnIndex = Int16.Parse(btn.Uid);

            //si le contenu du bouton est false
            if (!ListeCase.Single(c => c.Id == btnIndex).Value)
            {
                if (IsJoueur1)
                {
                    User1.Coup.Add(btnIndex);
                    ListeCase.Single(c => c.Id == btnIndex).Value = true;
                    ListeCase.Single(c => c.Id == btnIndex).Path = new Uri("Resources/Pictures/cross.png", UriKind.Relative);

                    if (CheckWin(User1.Coup))
                        EndGame(User1, User2, false);

                    IsJoueur1 = false;
                }
                else
                {
                    User2.Coup.Add(btnIndex);
                    ListeCase.Single(c => c.Id == btnIndex).Value = true;
                    ListeCase.Single(c => c.Id == btnIndex).Path = new Uri("Resources/Pictures/circle.png", UriKind.Relative);

                    if (CheckWin(User2.Coup))
                        EndGame(User2, User1, false);

                    IsJoueur1 = true;
                }

                //Partie nulle si plus de
                if (ListeCase.Count(c => c.Value == false) == 0)
                    EndGame(User1, User2, true);

            }
        }

        /// <summary>
        /// 
        /// </summary>
        private void EndGame(User userWin, User userLose, bool isMatchNul)
        {
            if (isMatchNul)
            {
                MessageBox.Show("Match nul !");
            }
            else
            {
                MessageBox.Show("Joueur " + userWin.Nom + " a gagné");
                _statsViewModel.AddStatToFile(userWin, userLose);
            }
        }

        /// <summary>
        /// Teste si la liste de coups est gagnante et retourne true sinon false
        /// </summary>
        /// <param name="liste">Liste des coups</param>
        /// <returns></returns>
        private bool CheckWin(List<int> liste)
        {
            if (liste.Count > 2)
            {
                for (int i = 0; i < liste.Count; i++)
                {
                    if (liste[0] + liste[1] + liste[2] == 15)
                    {
                        return true;
                    }
                    liste.Add(liste[0]);
                    liste.RemoveAt(0);
                }
            }
            return false;
        }

        /// <summary>
        /// Choisit aléatoirement le joueur qui débutera la partie.
        /// </summary>
        private bool GetFirstUser()
        {
            return new Random().Next(2) == 0;
        }
    }
}
