﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wpf_TicTacToe.Models;
using Newtonsoft.Json;
using System.IO;
using System.Windows;

namespace Wpf_TicTacToe.ViewModels
{
    public class MenuViewModel : BaseModel
    {
        //Variables publiques
        public Uri UriBackground
        {
            get => new Uri("Resources/Pictures/MenuBackground.png", UriKind.Relative);
        }
    }
}
