﻿using System;
using System.Collections.Generic;
using Wpf_TicTacToe.Models;

namespace Wpf_TicTacToe.ViewModels
{
    public class LobbyViewModel : BaseModel
    {
        List<string> _listPlayer = new List<string>();

        public List<string> ListPlayer
        {
            get => _listPlayer;
            set
            {
                _listPlayer = value;
                OnPropertyChanged();
            }
        }

        public Uri UriBackground
        {
            get => new Uri("Resources/Pictures/gameBackground.png", UriKind.Relative);
        }
    }
}
