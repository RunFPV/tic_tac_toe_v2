﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Threading.Tasks;
using TicTacToe.Models.Services.Hubs;

namespace Wpf_TicTacToe.Hub
{
    public class ClientHub
    {
        public static HubConnection Connection;
        public static string UserName;

        public ClientHub()
        {
            #region Initialize Connection
            Connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:44383/gameHub")
                .Build();

            Connection.Closed += async (error) =>
            {
                await Task.Delay(new Random().Next(0, 5) * 1000);
                await Connection.StartAsync();
            };
            #endregion
        }

        public static async void StartHubAsync(string userName)
        {
            UserName = userName;
            try
            {
                await Connection.StartAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async void ConnectAsync()
        {
            try
            {
                await Connection.InvokeAsync("Connect", UserName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async void DisconnectAsync()
        {
            try
            {
                await Connection.InvokeAsync("Disconnect");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async void RefleshListAsync()
        {
            try
            {
                await Connection.InvokeAsync("RefleshList");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async void FightRequestAsync(string namePlayerCall)
        {
            try
            {
                await Connection.InvokeAsync("FightRequest", namePlayerCall);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async void FightResponseAsync(Battle battle)
        {
            try
            {
                await Connection.InvokeAsync("FightResponse", battle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static async void EndGameAsync(Battle battle)
        {
            try
            {
                await Connection.InvokeAsync("EndGame", battle);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
